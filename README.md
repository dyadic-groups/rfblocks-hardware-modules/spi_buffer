# SPI Communication Buffer

## Features

- Buffers Clk, MISO and MOSI.
- Optional level translation on SPI control lines.

## License

[CERN-OHL-W v2.](https://ohwr.org/project/cernohl/wikis/Documents/CERN-OHL-version-2)
