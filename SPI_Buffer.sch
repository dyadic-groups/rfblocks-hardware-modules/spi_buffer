EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "SPI Buffer"
Date "2022-01-07"
Rev "1.1"
Comp "Dyadic Pty Ltd"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L SPI_Buffer-rescue:74LVC4T3144-project U1
U 1 1 5F530EFD
P 5775 2450
F 0 "U1" H 6275 1475 50  0000 C CNN
F 1 "74LVC4T3144" H 5775 2000 50  0000 C CNN
F 2 "Package_SO:TSSOP-14_4.4x5mm_P0.65mm" H 5825 1500 50  0001 L CNN
F 3 "" H 5775 2850 50  0001 C CNN
	1    5775 2450
	1    0    0    -1  
$EndComp
$Comp
L SPI_Buffer-rescue:Conn_02x08_Odd_Even-Connector_Generic-SPI_Buffer-rescue J1
U 1 1 5F533201
P 2650 4225
F 0 "J1" H 2700 4742 50  0000 C CNN
F 1 "Input" H 2700 4651 50  0000 C CNN
F 2 "Connector_IDC:IDC-Header_2x08_P2.54mm_Vertical" H 2650 4225 50  0001 C CNN
F 3 "~" H 2650 4225 50  0001 C CNN
	1    2650 4225
	1    0    0    -1  
$EndComp
$Comp
L SPI_Buffer-rescue:Conn_02x08_Odd_Even-Connector_Generic-SPI_Buffer-rescue J2
U 1 1 5F537B0B
P 8650 4225
F 0 "J2" H 8700 4742 50  0000 C CNN
F 1 "Output" H 8700 4651 50  0000 C CNN
F 2 "Connector_IDC:IDC-Header_2x08_P2.54mm_Vertical" H 8650 4225 50  0001 C CNN
F 3 "~" H 8650 4225 50  0001 C CNN
	1    8650 4225
	1    0    0    -1  
$EndComp
$Comp
L SPI_Buffer-rescue:VCC-power-SPI_Buffer-rescue #PWR02
U 1 1 5F53BD2D
P 5975 925
F 0 "#PWR02" H 5975 775 50  0001 C CNN
F 1 "VCC" H 5990 1098 50  0000 C CNN
F 2 "" H 5975 925 50  0001 C CNN
F 3 "" H 5975 925 50  0001 C CNN
	1    5975 925 
	1    0    0    -1  
$EndComp
$Comp
L SPI_Buffer-rescue:VAA-power-SPI_Buffer-rescue #PWR01
U 1 1 5F53C486
P 5575 925
F 0 "#PWR01" H 5575 775 50  0001 C CNN
F 1 "VAA" H 5590 1098 50  0000 C CNN
F 2 "" H 5575 925 50  0001 C CNN
F 3 "" H 5575 925 50  0001 C CNN
	1    5575 925 
	1    0    0    -1  
$EndComp
$Comp
L SPI_Buffer-rescue:C_Small-Device-SPI_Buffer-rescue C2
U 1 1 5F53ED43
P 6150 1150
F 0 "C2" H 6242 1196 50  0000 L CNN
F 1 "0.1u" H 6200 1050 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 6150 1150 50  0001 C CNN
F 3 "~" H 6150 1150 50  0001 C CNN
	1    6150 1150
	1    0    0    -1  
$EndComp
$Comp
L SPI_Buffer-rescue:C_Small-Device-SPI_Buffer-rescue C1
U 1 1 5F53F97B
P 5400 1150
F 0 "C1" H 5225 1225 50  0000 L CNN
F 1 "0.1u" H 5175 1075 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 5400 1150 50  0001 C CNN
F 3 "~" H 5400 1150 50  0001 C CNN
	1    5400 1150
	1    0    0    -1  
$EndComp
$Comp
L SPI_Buffer-rescue:GND-power-SPI_Buffer-rescue #PWR04
U 1 1 5F54148B
P 6150 1250
F 0 "#PWR04" H 6150 1000 50  0001 C CNN
F 1 "GND" H 6155 1077 50  0000 C CNN
F 2 "" H 6150 1250 50  0001 C CNN
F 3 "" H 6150 1250 50  0001 C CNN
	1    6150 1250
	1    0    0    -1  
$EndComp
$Comp
L SPI_Buffer-rescue:GND-power-SPI_Buffer-rescue #PWR03
U 1 1 5F542355
P 5400 1250
F 0 "#PWR03" H 5400 1000 50  0001 C CNN
F 1 "GND" H 5405 1077 50  0000 C CNN
F 2 "" H 5400 1250 50  0001 C CNN
F 3 "" H 5400 1250 50  0001 C CNN
	1    5400 1250
	1    0    0    -1  
$EndComp
Wire Wire Line
	5975 1350 5975 1000
Wire Wire Line
	5575 1350 5575 1000
Wire Wire Line
	5400 1050 5400 1000
Wire Wire Line
	5400 1000 5575 1000
Connection ~ 5575 1000
Wire Wire Line
	5575 1000 5575 925 
Wire Wire Line
	6150 1050 6150 1000
Wire Wire Line
	6150 1000 5975 1000
Connection ~ 5975 1000
Wire Wire Line
	5975 1000 5975 925 
Wire Wire Line
	5575 3550 5575 3625
Wire Wire Line
	5975 3625 5975 3550
$Comp
L SPI_Buffer-rescue:GND-power-SPI_Buffer-rescue #PWR05
U 1 1 5F54345A
P 5775 3625
F 0 "#PWR05" H 5775 3375 50  0001 C CNN
F 1 "GND" H 5780 3452 50  0000 C CNN
F 2 "" H 5775 3625 50  0001 C CNN
F 3 "" H 5775 3625 50  0001 C CNN
	1    5775 3625
	1    0    0    -1  
$EndComp
$Comp
L SPI_Buffer-rescue:GND-power-SPI_Buffer-rescue #PWR08
U 1 1 5F54AAF9
P 8450 1325
F 0 "#PWR08" H 8450 1075 50  0001 C CNN
F 1 "GND" H 8455 1152 50  0000 C CNN
F 2 "" H 8450 1325 50  0001 C CNN
F 3 "" H 8450 1325 50  0001 C CNN
	1    8450 1325
	1    0    0    -1  
$EndComp
$Comp
L SPI_Buffer-rescue:VAA-power-SPI_Buffer-rescue #PWR07
U 1 1 5F54AFB6
P 8450 875
F 0 "#PWR07" H 8450 725 50  0001 C CNN
F 1 "VAA" H 8465 1048 50  0000 C CNN
F 2 "" H 8450 875 50  0001 C CNN
F 3 "" H 8450 875 50  0001 C CNN
	1    8450 875 
	1    0    0    -1  
$EndComp
$Comp
L SPI_Buffer-rescue:VCC-power-SPI_Buffer-rescue #PWR06
U 1 1 5F54BD19
P 8225 875
F 0 "#PWR06" H 8225 725 50  0001 C CNN
F 1 "VCC" H 8240 1048 50  0000 C CNN
F 2 "" H 8225 875 50  0001 C CNN
F 3 "" H 8225 875 50  0001 C CNN
	1    8225 875 
	1    0    0    -1  
$EndComp
Wire Wire Line
	9200 1000 9150 1000
Wire Wire Line
	8450 1000 8450 875 
Wire Wire Line
	9200 1100 8725 1100
Wire Wire Line
	8225 1100 8225 875 
Wire Wire Line
	8450 1200 8450 1325
Wire Wire Line
	8950 3925 9550 3925
Wire Wire Line
	8950 4125 9550 4125
Wire Wire Line
	8950 4225 9550 4225
Wire Wire Line
	8950 4425 9550 4425
Wire Wire Line
	8950 4525 9550 4525
Text Label 9200 3925 0    50   ~ 0
mosi_b
Text Label 8175 4125 0    50   ~ 0
d0_b
Text Label 9200 4125 0    50   ~ 0
d1_b
Text Label 8175 4225 0    50   ~ 0
d2_b
Text Label 9200 4225 0    50   ~ 0
d3_b
Text Label 9200 4525 0    50   ~ 0
d8_b
Entry Wire Line
	9550 3925 9650 3825
Wire Bus Line
	9650 5075 7950 5075
Wire Wire Line
	8450 3925 8050 3925
Wire Wire Line
	8450 4025 8050 4025
Wire Wire Line
	8450 4125 8050 4125
Wire Wire Line
	8450 4225 8050 4225
Text Label 8175 3925 0    50   ~ 0
sck_b
Text Label 8175 4025 0    50   ~ 0
miso_b
Text Label 8175 4325 0    50   ~ 0
d4_b
Wire Wire Line
	8450 4325 8050 4325
Wire Wire Line
	8450 4425 8050 4425
Wire Wire Line
	8450 4525 8050 4525
Wire Wire Line
	8450 4625 8050 4625
Text Label 8175 4425 0    50   ~ 0
d5_b
Text Label 9200 4425 0    50   ~ 0
d6_b
Text Label 8175 4525 0    50   ~ 0
d7_b
Text Label 8175 4625 0    50   ~ 0
d9_b
Entry Wire Line
	9550 4125 9650 4025
Entry Wire Line
	9550 4225 9650 4125
Entry Wire Line
	9550 4425 9650 4325
Entry Wire Line
	9550 4525 9650 4425
Wire Wire Line
	6575 2050 6925 2050
Wire Wire Line
	6575 2250 6925 2250
Wire Wire Line
	6575 2650 6925 2650
Text Label 6625 2050 0    50   ~ 0
sck_b
Text Label 6625 2250 0    50   ~ 0
mosi_b
Text Label 6625 2650 0    50   ~ 0
miso_b
Entry Wire Line
	6925 2050 7025 2150
Entry Wire Line
	6925 2250 7025 2350
Entry Wire Line
	6925 2650 7025 2750
NoConn ~ 6575 2450
Entry Wire Line
	3550 3925 3650 4025
Wire Wire Line
	2950 3925 3550 3925
Wire Wire Line
	2950 4125 3550 4125
Wire Wire Line
	2950 4225 3550 4225
Wire Wire Line
	2950 4425 3550 4425
Wire Wire Line
	2950 4525 3550 4525
Text Label 3200 3925 0    50   ~ 0
mosi_a
Text Label 2175 4225 0    50   ~ 0
d2_a
Text Label 2175 4325 0    50   ~ 0
d4_a
Text Label 3200 4425 0    50   ~ 0
d6_a
Text Label 3200 4525 0    50   ~ 0
d8_a
Text Label 3200 4125 0    50   ~ 0
d1_a
Text Label 3200 4225 0    50   ~ 0
d3_a
Text Label 2175 4425 0    50   ~ 0
d5_a
Text Label 2175 4525 0    50   ~ 0
d7_a
Text Label 2175 4625 0    50   ~ 0
d9_a
Entry Wire Line
	4525 1950 4625 2050
Wire Wire Line
	4975 2050 4625 2050
Wire Wire Line
	4975 2250 4625 2250
Wire Wire Line
	4975 2650 4625 2650
Entry Wire Line
	4525 2150 4625 2250
Entry Wire Line
	4525 2550 4625 2650
Text Label 4700 2050 0    50   ~ 0
sck_a
Text Label 4700 2250 0    50   ~ 0
mosi_a
Text Label 4700 2650 0    50   ~ 0
miso_a
NoConn ~ 4975 2450
Entry Wire Line
	3550 4125 3650 4225
Entry Wire Line
	3550 4225 3650 4325
Entry Wire Line
	3550 4425 3650 4525
Entry Wire Line
	3550 4525 3650 4625
$Comp
L SPI_Buffer-rescue:PWR_FLAG-power-SPI_Buffer-rescue #FLG0101
U 1 1 5F5F9606
P 9150 1000
F 0 "#FLG0101" H 9150 1075 50  0001 C CNN
F 1 "PWR_FLAG" H 9150 1173 50  0000 C CNN
F 2 "" H 9150 1000 50  0001 C CNN
F 3 "~" H 9150 1000 50  0001 C CNN
	1    9150 1000
	1    0    0    -1  
$EndComp
Connection ~ 9150 1000
$Comp
L SPI_Buffer-rescue:PWR_FLAG-power-SPI_Buffer-rescue #FLG0102
U 1 1 5F6002E2
P 8725 1100
F 0 "#FLG0102" H 8725 1175 50  0001 C CNN
F 1 "PWR_FLAG" H 8725 1273 50  0000 C CNN
F 2 "" H 8725 1100 50  0001 C CNN
F 3 "~" H 8725 1100 50  0001 C CNN
	1    8725 1100
	1    0    0    -1  
$EndComp
Connection ~ 8725 1100
Wire Wire Line
	8725 1100 8225 1100
Wire Wire Line
	8450 1000 9150 1000
Wire Wire Line
	9200 1200 8725 1200
$Comp
L SPI_Buffer-rescue:PWR_FLAG-power-SPI_Buffer-rescue #FLG0103
U 1 1 5F606F90
P 8725 1200
F 0 "#FLG0103" H 8725 1275 50  0001 C CNN
F 1 "PWR_FLAG" H 8725 1373 50  0000 C CNN
F 2 "" H 8725 1200 50  0001 C CNN
F 3 "~" H 8725 1200 50  0001 C CNN
	1    8725 1200
	-1   0    0    1   
$EndComp
Connection ~ 8725 1200
Wire Wire Line
	8725 1200 8450 1200
Connection ~ 5775 3625
Wire Wire Line
	5775 3625 5975 3625
Wire Wire Line
	5575 3625 5775 3625
Wire Bus Line
	3650 3925 4100 3925
Wire Wire Line
	5725 4450 6200 4450
Wire Bus Line
	7025 3575 9650 3575
Wire Bus Line
	7950 3925 7275 3925
Entry Wire Line
	7950 4025 8050 3925
Entry Wire Line
	7950 4125 8050 4025
Entry Wire Line
	7950 4225 8050 4125
Entry Wire Line
	7950 4325 8050 4225
Entry Wire Line
	7950 4425 8050 4325
Entry Wire Line
	7950 4525 8050 4425
Entry Wire Line
	7950 4625 8050 4525
Entry Wire Line
	7950 4725 8050 4625
Entry Wire Line
	7175 4450 7275 4350
Wire Wire Line
	6700 4450 7175 4450
Wire Wire Line
	6700 4550 7175 4550
Wire Wire Line
	6700 4650 7175 4650
Wire Wire Line
	6700 4750 7175 4750
Wire Wire Line
	6700 4850 7175 4850
Wire Wire Line
	6700 4950 7175 4950
Wire Wire Line
	6700 5050 7175 5050
Wire Wire Line
	6700 5150 7175 5150
Wire Wire Line
	6700 5250 7175 5250
Wire Wire Line
	6700 5350 7175 5350
Wire Wire Line
	6200 4550 5725 4550
Wire Wire Line
	6200 4650 5725 4650
Wire Wire Line
	6200 4750 5725 4750
Wire Wire Line
	6200 4850 5725 4850
Wire Wire Line
	6200 4950 5725 4950
Wire Wire Line
	6200 5050 5725 5050
Wire Wire Line
	6200 5150 5725 5150
Wire Wire Line
	6200 5250 5725 5250
Wire Wire Line
	6200 5350 5725 5350
Entry Wire Line
	5625 4650 5725 4550
Entry Wire Line
	5625 4550 5725 4450
Entry Wire Line
	5625 4750 5725 4650
Entry Wire Line
	5625 4850 5725 4750
Entry Wire Line
	5625 4950 5725 4850
Entry Wire Line
	5625 5050 5725 4950
Entry Wire Line
	5625 5150 5725 5050
Entry Wire Line
	5625 5250 5725 5150
Entry Wire Line
	5625 5350 5725 5250
Entry Wire Line
	5625 5450 5725 5350
Entry Wire Line
	7175 4550 7275 4450
Entry Wire Line
	7175 4650 7275 4550
Entry Wire Line
	7175 4750 7275 4650
Entry Wire Line
	7175 4850 7275 4750
Entry Wire Line
	7175 4950 7275 4850
Entry Wire Line
	7175 5050 7275 4950
Entry Wire Line
	7175 5150 7275 5050
Entry Wire Line
	7175 5250 7275 5150
Entry Wire Line
	7175 5350 7275 5250
Text Label 6750 4450 0    50   ~ 0
d0_b
Text Label 6750 4550 0    50   ~ 0
d1_b
Text Label 6750 4650 0    50   ~ 0
d2_b
Text Label 6750 4750 0    50   ~ 0
d3_b
Text Label 6750 4850 0    50   ~ 0
d4_b
Text Label 6750 4950 0    50   ~ 0
d5_b
Text Label 6750 5050 0    50   ~ 0
d6_b
Text Label 6750 5150 0    50   ~ 0
d7_b
Text Label 6750 5250 0    50   ~ 0
d8_b
Text Label 6750 5350 0    50   ~ 0
d9_b
Text Label 5900 4450 0    50   ~ 0
d0_a
Text Label 5900 4550 0    50   ~ 0
d1_a
Text Label 5900 4650 0    50   ~ 0
d2_a
Text Label 5900 4750 0    50   ~ 0
d3_a
Text Label 5900 4850 0    50   ~ 0
d4_a
Text Label 5900 4950 0    50   ~ 0
d5_a
Text Label 5900 5050 0    50   ~ 0
d6_a
Text Label 5900 5150 0    50   ~ 0
d7_a
Text Label 5900 5250 0    50   ~ 0
d8_a
Text Label 5900 5350 0    50   ~ 0
d9_a
$Comp
L SPI_Buffer-rescue:R_Small-Device-SPI_Buffer-rescue R1
U 1 1 5F752DFD
P 4775 3200
F 0 "R1" H 4834 3246 50  0000 L CNN
F 1 "10K" H 4834 3155 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 4775 3200 50  0001 C CNN
F 3 "~" H 4775 3200 50  0001 C CNN
	1    4775 3200
	1    0    0    -1  
$EndComp
$Comp
L SPI_Buffer-rescue:GND-power-SPI_Buffer-rescue #PWR0101
U 1 1 5F75363E
P 4775 3300
F 0 "#PWR0101" H 4775 3050 50  0001 C CNN
F 1 "GND" H 4780 3127 50  0000 C CNN
F 2 "" H 4775 3300 50  0001 C CNN
F 3 "" H 4775 3300 50  0001 C CNN
	1    4775 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	4975 3000 4775 3000
Wire Wire Line
	4775 3000 4775 3100
$Comp
L SPI_Buffer-rescue:Conn_01x04_Male-Connector-SPI_Buffer-rescue J5
U 1 1 5F75C8E3
P 9400 1100
F 0 "J5" H 9372 1074 50  0000 R CNN
F 1 "Pwr/Ctl" H 9372 983 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 9400 1100 50  0001 C CNN
F 3 "~" H 9400 1100 50  0001 C CNN
	1    9400 1100
	-1   0    0    -1  
$EndComp
Wire Wire Line
	9200 1300 9075 1300
Wire Wire Line
	9075 1300 9075 1500
Text GLabel 9075 1500 3    50   Input ~ 0
OE
Text GLabel 4325 3000 0    50   Input ~ 0
OE
Wire Wire Line
	4325 3000 4775 3000
Connection ~ 4775 3000
NoConn ~ 6575 3000
Entry Wire Line
	4100 4350 4200 4250
Connection ~ 4100 3925
Wire Bus Line
	4100 3925 5625 3925
Wire Wire Line
	4700 4250 4200 4250
Wire Wire Line
	4700 4350 4200 4350
Wire Wire Line
	4700 4450 4200 4450
Entry Wire Line
	4100 4450 4200 4350
Entry Wire Line
	4100 4550 4200 4450
Text Label 4375 4250 0    50   ~ 0
sck_a
Text Label 4375 4350 0    50   ~ 0
mosi_a
Text Label 4375 4450 0    50   ~ 0
miso_a
$Comp
L SPI_Buffer-rescue:GND-power-SPI_Buffer-rescue #PWR010
U 1 1 5F80FC46
P 3075 4800
F 0 "#PWR010" H 3075 4550 50  0001 C CNN
F 1 "GND" H 3080 4627 50  0000 C CNN
F 2 "" H 3075 4800 50  0001 C CNN
F 3 "" H 3075 4800 50  0001 C CNN
	1    3075 4800
	1    0    0    -1  
$EndComp
Wire Wire Line
	2950 4025 3075 4025
Wire Wire Line
	2950 4625 3075 4625
Connection ~ 3075 4625
Wire Wire Line
	3075 4625 3075 4800
Wire Wire Line
	2950 4325 3075 4325
Wire Wire Line
	3075 4025 3075 4325
Connection ~ 3075 4325
Wire Wire Line
	3075 4325 3075 4625
$Comp
L SPI_Buffer-rescue:GND-power-SPI_Buffer-rescue #PWR011
U 1 1 5F8F274A
P 9100 4800
F 0 "#PWR011" H 9100 4550 50  0001 C CNN
F 1 "GND" H 9105 4627 50  0000 C CNN
F 2 "" H 9100 4800 50  0001 C CNN
F 3 "" H 9100 4800 50  0001 C CNN
	1    9100 4800
	1    0    0    -1  
$EndComp
Wire Wire Line
	8950 4025 9100 4025
Wire Wire Line
	9100 4025 9100 4325
Wire Wire Line
	8950 4325 9100 4325
Connection ~ 9100 4325
Wire Wire Line
	9100 4325 9100 4625
Wire Wire Line
	8950 4625 9100 4625
Connection ~ 9100 4625
Wire Wire Line
	9100 4625 9100 4800
$Comp
L SPI_Buffer-rescue:Conn_02x10_Odd_Even-Connector_Generic-SPI_Buffer-rescue J4
U 1 1 5F9107B5
P 6400 4850
F 0 "J4" H 6450 5467 50  0000 C CNN
F 1 "Bridge" H 6450 5376 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x10_P2.54mm_Vertical" H 6400 4850 50  0001 C CNN
F 3 "~" H 6400 4850 50  0001 C CNN
	1    6400 4850
	1    0    0    -1  
$EndComp
$Comp
L SPI_Buffer-rescue:Conn_01x04-Connector_Generic-SPI_Buffer-rescue J3
U 1 1 5F943C7A
P 4900 4350
F 0 "J3" H 4980 4342 50  0000 L CNN
F 1 "SPI" H 4980 4251 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 4900 4350 50  0001 C CNN
F 3 "~" H 4900 4350 50  0001 C CNN
	1    4900 4350
	1    0    0    -1  
$EndComp
$Comp
L SPI_Buffer-rescue:GND-power-SPI_Buffer-rescue #PWR09
U 1 1 5F949294
P 4550 4725
F 0 "#PWR09" H 4550 4475 50  0001 C CNN
F 1 "GND" H 4555 4552 50  0000 C CNN
F 2 "" H 4550 4725 50  0001 C CNN
F 3 "" H 4550 4725 50  0001 C CNN
	1    4550 4725
	1    0    0    -1  
$EndComp
Wire Wire Line
	4700 4550 4550 4550
Wire Wire Line
	4550 4550 4550 4725
Text Label 2175 4125 0    50   ~ 0
d0_a
Text Label 2175 4025 0    50   ~ 0
miso_a
Text Label 2175 3925 0    50   ~ 0
sck_a
Wire Wire Line
	2450 4325 2050 4325
Wire Wire Line
	2450 4625 2050 4625
Wire Wire Line
	2450 4525 2050 4525
Wire Wire Line
	2450 4425 2050 4425
Wire Wire Line
	2450 4225 2050 4225
Wire Wire Line
	2450 4125 2050 4125
Wire Wire Line
	2450 4025 2050 4025
Wire Wire Line
	2450 3925 2050 3925
Wire Bus Line
	3650 5075 1950 5075
Entry Wire Line
	1950 4225 2050 4325
Entry Wire Line
	1950 4525 2050 4625
Entry Wire Line
	1950 4425 2050 4525
Entry Wire Line
	1950 4325 2050 4425
Entry Wire Line
	1950 4125 2050 4225
Entry Wire Line
	1950 4025 2050 4125
Entry Wire Line
	1950 3925 2050 4025
Entry Wire Line
	1950 3825 2050 3925
Wire Bus Line
	1950 3575 4525 3575
Connection ~ 1950 5075
$Comp
L Device:C_Small C8
U 1 1 61E6F14E
P 2250 6275
F 0 "C8" H 2275 6350 50  0000 L CNN
F 1 "10p" H 2075 6200 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 2250 6275 50  0001 C CNN
F 3 "~" H 2250 6275 50  0001 C CNN
	1    2250 6275
	1    0    0    -1  
$EndComp
Entry Wire Line
	1950 5300 2050 5400
Entry Wire Line
	1950 5400 2050 5500
Entry Wire Line
	1950 5500 2050 5600
Entry Wire Line
	1950 5600 2050 5700
Entry Wire Line
	1950 5700 2050 5800
Entry Wire Line
	1950 5800 2050 5900
Entry Wire Line
	1950 5900 2050 6000
Entry Wire Line
	1950 6000 2050 6100
$Comp
L Device:C_Small C10
U 1 1 61E71E25
P 2450 6475
F 0 "C10" H 2475 6550 50  0000 L CNN
F 1 "10p" H 2275 6400 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 2450 6475 50  0001 C CNN
F 3 "~" H 2450 6475 50  0001 C CNN
	1    2450 6475
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C12
U 1 1 61E721DA
P 2650 6675
F 0 "C12" H 2675 6750 50  0000 L CNN
F 1 "10p" H 2475 6600 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 2650 6675 50  0001 C CNN
F 3 "~" H 2650 6675 50  0001 C CNN
	1    2650 6675
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C14
U 1 1 61E72E2F
P 2850 6875
F 0 "C14" H 2875 6950 50  0000 L CNN
F 1 "10p" H 2675 6800 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 2850 6875 50  0001 C CNN
F 3 "~" H 2850 6875 50  0001 C CNN
	1    2850 6875
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C9
U 1 1 61E735AA
P 3050 6275
F 0 "C9" H 3075 6350 50  0000 L CNN
F 1 "10p" H 2875 6200 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 3050 6275 50  0001 C CNN
F 3 "~" H 3050 6275 50  0001 C CNN
	1    3050 6275
	1    0    0    -1  
$EndComp
Wire Wire Line
	2050 6100 2250 6100
Wire Wire Line
	2250 6100 2250 6175
Wire Wire Line
	2450 6000 2450 6375
Wire Wire Line
	2050 6000 2450 6000
Wire Wire Line
	2050 5900 2650 5900
Wire Wire Line
	2650 5900 2650 6575
Wire Wire Line
	2050 5800 2850 5800
Wire Wire Line
	2850 5800 2850 6775
$Comp
L Device:C_Small C11
U 1 1 61E99164
P 3250 6475
F 0 "C11" H 3275 6550 50  0000 L CNN
F 1 "10p" H 3075 6400 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 3250 6475 50  0001 C CNN
F 3 "~" H 3250 6475 50  0001 C CNN
	1    3250 6475
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C13
U 1 1 61E99C31
P 3450 6675
F 0 "C13" H 3475 6750 50  0000 L CNN
F 1 "10p" H 3275 6600 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 3450 6675 50  0001 C CNN
F 3 "~" H 3450 6675 50  0001 C CNN
	1    3450 6675
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C15
U 1 1 61E99E5F
P 3650 6875
F 0 "C15" H 3675 6950 50  0000 L CNN
F 1 "10p" H 3475 6800 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 3650 6875 50  0001 C CNN
F 3 "~" H 3650 6875 50  0001 C CNN
	1    3650 6875
	1    0    0    -1  
$EndComp
Wire Wire Line
	2050 5700 3050 5700
Wire Wire Line
	3050 5700 3050 6175
Wire Wire Line
	2050 5600 3250 5600
Wire Wire Line
	3250 5600 3250 6375
Wire Wire Line
	2050 5500 3450 5500
Wire Wire Line
	3450 5500 3450 6575
Wire Wire Line
	2050 5400 3650 5400
Wire Wire Line
	3650 5400 3650 6775
Wire Wire Line
	2250 6375 2250 7075
Wire Wire Line
	2250 7075 2450 7075
Wire Wire Line
	3650 7075 3650 6975
Wire Wire Line
	3450 6775 3450 7075
Connection ~ 3450 7075
Wire Wire Line
	3450 7075 3650 7075
Wire Wire Line
	3250 6575 3250 7075
Connection ~ 3250 7075
Wire Wire Line
	3250 7075 3450 7075
Wire Wire Line
	3050 6375 3050 7075
Connection ~ 3050 7075
Wire Wire Line
	3050 7075 3250 7075
Wire Wire Line
	2650 6775 2650 7075
Connection ~ 2650 7075
Wire Wire Line
	2650 7075 2850 7075
Wire Wire Line
	2450 6575 2450 7075
Connection ~ 2450 7075
Wire Wire Line
	2450 7075 2650 7075
Wire Wire Line
	2850 6975 2850 7075
Connection ~ 2850 7075
Wire Wire Line
	2850 7075 3050 7075
$Comp
L SPI_Buffer-rescue:GND-power-SPI_Buffer-rescue #PWR013
U 1 1 61EDDB7F
P 3050 7075
F 0 "#PWR013" H 3050 6825 50  0001 C CNN
F 1 "GND" H 3055 6902 50  0000 C CNN
F 2 "" H 3050 7075 50  0001 C CNN
F 3 "" H 3050 7075 50  0001 C CNN
	1    3050 7075
	1    0    0    -1  
$EndComp
Text Label 2175 5400 0    50   ~ 0
sck_a
Text Label 2175 5500 0    50   ~ 0
miso_a
Text Label 2175 5600 0    50   ~ 0
d0_a
Text Label 2175 5700 0    50   ~ 0
d2_a
Text Label 2175 5800 0    50   ~ 0
d4_a
Text Label 2175 5900 0    50   ~ 0
d5_a
Text Label 2175 6000 0    50   ~ 0
d7_a
Text Label 2175 6100 0    50   ~ 0
d9_a
Entry Wire Line
	5525 5050 5625 4950
Entry Wire Line
	5525 5150 5625 5050
Entry Wire Line
	5525 5250 5625 5150
Entry Wire Line
	5525 5350 5625 5250
Entry Wire Line
	5525 5450 5625 5350
$Comp
L Device:C_Small C4
U 1 1 61EF2330
P 4575 5600
F 0 "C4" H 4600 5675 50  0000 L CNN
F 1 "10p" H 4400 5525 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 4575 5600 50  0001 C CNN
F 3 "~" H 4575 5600 50  0001 C CNN
	1    4575 5600
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C5
U 1 1 61EF233A
P 4775 5800
F 0 "C5" H 4800 5875 50  0000 L CNN
F 1 "10p" H 4600 5725 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 4775 5800 50  0001 C CNN
F 3 "~" H 4775 5800 50  0001 C CNN
	1    4775 5800
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C6
U 1 1 61EF2344
P 4975 6000
F 0 "C6" H 5000 6075 50  0000 L CNN
F 1 "10p" H 4800 5925 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 4975 6000 50  0001 C CNN
F 3 "~" H 4975 6000 50  0001 C CNN
	1    4975 6000
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C7
U 1 1 61EF234E
P 5175 6200
F 0 "C7" H 5200 6275 50  0000 L CNN
F 1 "10p" H 5000 6125 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 5175 6200 50  0001 C CNN
F 3 "~" H 5175 6200 50  0001 C CNN
	1    5175 6200
	1    0    0    -1  
$EndComp
Wire Wire Line
	5175 6400 5175 6300
Wire Wire Line
	4975 6100 4975 6400
Wire Wire Line
	4775 5900 4775 6400
Wire Wire Line
	4575 5700 4575 6400
$Comp
L Device:C_Small C3
U 1 1 61F18BE3
P 4375 5450
F 0 "C3" H 4400 5525 50  0000 L CNN
F 1 "10p" H 4200 5375 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 4375 5450 50  0001 C CNN
F 3 "~" H 4375 5450 50  0001 C CNN
	1    4375 5450
	1    0    0    -1  
$EndComp
Wire Wire Line
	5525 5350 4975 5350
Wire Wire Line
	4975 5350 4975 5900
Wire Wire Line
	5525 5450 5175 5450
Wire Wire Line
	5175 5450 5175 6100
Wire Wire Line
	4375 5550 4375 6400
Wire Wire Line
	4375 6400 4575 6400
Connection ~ 4575 6400
Wire Wire Line
	4575 6400 4775 6400
Connection ~ 4775 6400
Wire Wire Line
	4775 6400 4975 6400
Connection ~ 4975 6400
Wire Wire Line
	4975 6400 5175 6400
$Comp
L SPI_Buffer-rescue:GND-power-SPI_Buffer-rescue #PWR012
U 1 1 61F5E990
P 4775 6400
F 0 "#PWR012" H 4775 6150 50  0001 C CNN
F 1 "GND" H 4780 6227 50  0000 C CNN
F 2 "" H 4775 6400 50  0001 C CNN
F 3 "" H 4775 6400 50  0001 C CNN
	1    4775 6400
	1    0    0    -1  
$EndComp
Text Label 5225 5050 0    50   ~ 0
mosi_a
Text Label 5225 5150 0    50   ~ 0
d1_a
Wire Wire Line
	4775 5250 4775 5700
Wire Wire Line
	4775 5250 5525 5250
Wire Wire Line
	4575 5150 4575 5500
Wire Wire Line
	4575 5150 5525 5150
Wire Wire Line
	4375 5050 4375 5350
Wire Wire Line
	4375 5050 5525 5050
Text Label 5225 5250 0    50   ~ 0
d3_a
Text Label 5225 5350 0    50   ~ 0
d6_a
Text Label 5225 5450 0    50   ~ 0
d8_a
Wire Bus Line
	4100 3925 4100 4650
Wire Bus Line
	4525 1925 4525 3575
Wire Bus Line
	7025 2050 7025 3575
Wire Bus Line
	9650 3575 9650 5075
Wire Bus Line
	3650 3925 3650 5075
Wire Bus Line
	1950 5075 1950 6500
Wire Bus Line
	7275 3925 7275 5575
Wire Bus Line
	7950 3925 7950 5075
Wire Bus Line
	5625 3925 5625 6450
Wire Bus Line
	1950 3575 1950 5075
$EndSCHEMATC
